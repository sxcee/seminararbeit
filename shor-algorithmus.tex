1994 hat \texttt{Peter Shor} seinen Algorithmus vorgestellt, indem er die Quanteninformatik benutzt, um die Faktorisierung sehr großer natürlicher Zahlen zu beschleunigen. Wie bereits aus den vorhergehenden Kapitel ersichtlich wurde, stellt dies selbst in unserer heutigen Zeit ein sehr großes Problem dar. \\

Während \texttt{Shor} viele der klassischen Methoden benutzt, 'lagert' er die Bestimmung der Ordnung einer Zahl in einem Zahlenraum in einen Quantencomputer aus. Mit einem ausreichend großen Computer kann dieser Algorithmus die Faktorisierung mit einem Zeitbedarf von $\mathcal{O}(n^3 log(n))$ berechnen und ist damit wesentlich schneller, als bisher bekannte Faktorisierungsalgorithmen. \\

Mit den bereits gelegten Grundlagen möchte ich an dieser Stelle direkt in den Algorithmus springen, um diesen im Anschluss näher zu erläutern. Ich werde mich bei der Herleitung an die Lösungswege in Quelle \cite{hirvensalo2013quantum} halten und diese verwenden. \\

Sei $n$ eine ungerade und zusammengesetzte natürliche Zahl, welche ebenfalls keine perfekte Potenz ist\footnote{}. Gesucht sei dazu ein nicht-trivialer Teiler dieser Zahl $n$. Man suche sich eine beliebige Zahl $z$ aus $\mathbb{F}_{n}$ mit den Eigenschaften $z < 1$ und $z \neq n$. \\

Nachdem sichergegangen wurde, das der größte gemeinsame Teile von $n$ und $z$ $ggT(z, n) = 1$ ist, also kein gemeinsamer Teiler, außer des trivialen Teilers $1$ existiert, muss die Ordnung der Zahl $z$ in $\mathbb{F}_{n}$ berechnet werden. Dies ist der Schritt, den \texttt{Shor} in den Quantencomputer auslagert. Auf diesen Schritt werde ich zu einem späteren Zeitpunkt noch einmal näher eingehen. \\

Sei $r$ die Ordnung von $z$ in $\mathbb{F}_{n}$, so gilt
\begin{equation}
\label{eq:ordnung}
	z^r = z^0 \equiv 1 \quad mod \quad n
\end{equation}

Aus Gleichung \ref{eq:ordnung} wird durch umstellen ersichtlich, dass $z^r - 1$ durch $n$ teilbar ist. Daraus folgt ebenfalls $z^r - 1 = (z^{\frac{r}{2}} - 1) \cdot (z^{\frac{r}{2}} + 1)$, was bedeutet, dass sich mit $ggT(z^{\frac{r}{2}} - 1, n)$ ein Faktor der Zahl $n$ berechnet werden kann. \\

Bei dieser Berechnung können zwei Probleme auftreten. Ist das Ergebnis $ggT(z^{\frac{r}{2}} - 1, n)$ ein trivialer Teiler, also $1$ oder $n$ selbst, so muss das Verfahren mit einer neuen zufälligen Zahl $z$ wiederholt werden. Gleiches gilt auch, wenn $z$ eine ungerade Zahl ist. \\

Eine Beispielimplementierung könnte folgendermaßen aussehen. Man beachte, dass ich den im Quantencomputer berechneten Schritt im Pseudocode nicht näher erläutern möchte, sodass ich ihn im Weiteren nur noch als \texttt{getOrderFromQuantumComputer()} bezeichnen werde. \\

\begin{lstlisting}[caption={Beispielimplementierung des Shor Algorithmus}]
n: natuerliche Zahl, zusammengesetzt, kein perfekter Teiler
z: zufaellig ausgewaehlte Zahl mit z > 1 und z != n
func shor_algorithmn(n, z):
	if ggT(n, z) == 1:
		order := getOrderFromQuantumComputer(n, z)
		if !(isOdd(r) || isTrivialFactor(r)):
			tmp := pow(z, order/2)
			factor_1 := ggT(tmp, n)
			factor_2 := n / factor_1
		else:
			return 0
	else:
		return 0
\end{lstlisting}

Wie im Pseudocode sehr schon nachzuvollziehen ist, erhalten wir genau dann ein Ergebnis, also einen nicht-trivialen Teiler der Zahl $n$, wenn die berechnete Ordnung $r$ keinen trivialen Teiler darstellt, oder wenn die Ordnung ungerade ist. \\

Nun möchte ich aber näher auf den eigentlichen Schritt \texttt{getOrderFromQuantumComputer()} eingehen. In diesem befindet sich nämlich die ganze \textsf{Magie}, weswegen das ganze Verfahren so effizient und revolutionär abläuft. \\

Shor hat herausgefunden, dass man mittels einer Fouriertransformation, den ganzen Faktorisierungsprozess auf nur lediglich die Berechnung einer Ordnung reduzieren kann. Dies basiert auf mehreren Definitionen\footnote{\cite{hirvensalo2013quantum}} wie zum Beispiel

\begin{center}
	\textsf{Für eine natürliche Zahl $n$ gilt $n = p_{1}^{e_{1}} \cdots p_{k}^{e_{k}}$. $p_i$ seien hierbei Primfaktoren von $n$ mit $k \geq 2$. Genau dann ist die Wahrscheinlichkeit, dass $r = ord_{n}(a)$ gerade ist und $a^{\frac{r}{2}} -1 \quad (mod \quad n)$ mindestens $\frac{3}{8}$.}
\end{center}

Ebenfalls gilt

\begin{center}
	\textsf{Mit den selben Angaben ist die Wahrscheinlichkeit, dass $r = ord_{n}(a)$ ungerade und dass $a^{\frac{r}{2}} -1 \quad (mod \quad n)$ maximal $\frac{1}{2^{k} - 1}$.}
\end{center}

Mit diesen Definitionen lässt sich beweisen, dass jedes $n$ mindestens zwei Primfaktoren besitzen muss. Wenn man nun das Problem die Ordnung zu finden auf das Finden der Periode einer Funktion reduziert mit $f: \mathbb{Z} \rightarrow \mathbb{Z}_{n}$, also $f(k) = a^k \quad mod \quad n$, so sieht man, dass die Ordnung gleich der Periode ist, wenn $a^r = a^0 \equiv 1 \quad mod \quad n$. Um die Periode eine Funktion herauszufinden, kann man wie bereits beschrieben eine Fouriertransformation auf die Funktion anwenden. Wie man nun mit dieser Ausgangssituation auf die Ordnung einer Zahl, mit Umweg über die Periode einer Funktion $f$, kommt werde ich nun im folgenden Teil meiner Seminararbeit erläutern. \\

\textbf{Die Fouriertransformation} \\
Da man die Fouriertransformation natürlich nicht über komplett $\mathbb{Z}$ berechnen kann, wählt man sich ein $\mathbb{Z}_m$ mit \{0, 1, ... m - 1\} aus, sodass die Periode auf jeden Fall reinpassen wird. Per Definition kann die Periode bzw. die Ordnung einer Zahl nicht größer sein, als der Zahlenraum (in diesem Fall $r$ mit $r = ord_{n}(a)$ kann nicht größer sein als $n$). Im allgemeinen wählt man $m = 2^l$ mit $l$ gleich der Anzahl an Quantenbits, die benötigt werden um $n$ darzustellen. \\

Im folgenden werde ich die fünf Schritte stichwortartig durchgehen, die benötigt werden in einem Quantencomputer eine Ordnung $r$ zu berechnen.\\

\texttt{Schritt 1:} \\
Zuerst muss man die Superpositionen vorbereiten. Dies ermöglicht uns hoch parallelisiert die Berechnung durchzuführen. Anschließend wird das QBit-Register durch eine Hadamard-Transformation in $\mathbb{Z}_m$ überführt. \\

Beginnend mit $\vert 0 \rangle \vert 0 \rangle$ erzeugen wir also folgende Form
\begin{equation*}
	\frac{1}{\sqrt{m}} \sum_{k = 0}^{m - 1} \vert k \rangle \vert 0 \rangle
\end{equation*}

\texttt{Schritt 2:} \\
In diesem Schritt wird nun $k$ auf $a^k$ abgebildet. Hier wird also eine diskrete Exponentialfunktion implementiert.
\begin{equation}
	\label{eq:hamarad}
	\text{aus } k \mapsto a^k \text{ folgt also } \frac{1}{\sqrt{m}}\sum_{k = 0}^{m - 1} \vert k \rangle \vert a^k \rangle
\end{equation}
Da nun aber $k \mapsto a^k \quad mod \quad n$ eine Periode r besitzt, lässt sich Gleichung \ref{eq:hamarad} auch folgendermaßen darstellen
\begin{equation}
	\label{eq:hamaradumgeformt}
	\frac{1}{\sqrt{m}}\sum_{l = 0}^{r - 1} \sum_{q = 0}^{s_l} \vert qr + l \rangle \vert a^l \rangle
\end{equation}

\texttt{Schritt 3:} \\
Wendet man nun auf Gleichung \ref{eq:hamaradumgeformt} eine inverse Fouriertransformation auf $\mathbb{Z}_m$ an, so erhält man die folgende Gleichung, aus der man dann, in ausgerechneter Form, im nächsten Schritt schon fast das Ergebnis auslesen kann.
\begin{equation*}
	\frac{1}{\sqrt{m}}\sum_{l = 0}^{r - 1}\sum_{q = 0}^{s_l} \frac{1}{\sqrt{m}} \sum_{p = 0}^{m - 1} e^{\frac{2\pi ip(qr + l)}{m}} \vert p\rangle \vert a^l \rangle
\end{equation*}
\begin{equation*}
	= \frac{1}{\sqrt{m}}\sum_{l = 0}^{r - 1}\sum_{p = 0}^{m - 1} e^{\frac{2\pi ipl}{m}} \sum_{q = 0}^{s} e^{\frac{2\pi iprq}{m}} \vert p\rangle \vert a^l \rangle
\end{equation*}
\texttt{Schritt 4:} \\
Dieser Schritt befasst sich nun damit, das Ergebnis der inversen Fouriertransformation näher zu betrachten um geeignete Werte für $p$ zu bekommen. Nun gilt es die Konvergenzen	$\frac{p_i}{q_i}$ von $\frac{p}{m}$ zu finden. Hierzu kann man den Euklid-Algorithmus verwenden, um das kleinste $q_i$ zu finden, sodass $a^{q_i} \equiv 1 /quad mod \quad n$ gilt. Den Euklid-Algorithmus möchte ich an dieser Stelle nicht näher erläutern. \\

Ein Beispiel einer solchen Rechnung könnte folgendermaßen aussehen\footnote{Die Rechnung ist aus \cite{hirvensalo2013quantum}}. Es sein $n = 15$ und $a = 7$. Für $m$ wählen wir $m = 16$.

\texttt{Schritt 1:} \\
\begin{equation*}
\frac{1}{4}\sum_{k = 0}^{15} \vert k \rangle \vert 0 \rangle
\end{equation*}
\texttt{Schritt 2:} \\
$\text{Berechne } k \mapsto 7^k \text{ mod 15}$
\begin{equation*}
	\frac{1}{4}(\vert 0\rangle \vert 1\rangle +
							\vert 1\rangle \vert 7\rangle +
							\vert 2\rangle \vert 4\rangle +
							\vert 3\rangle \vert 13\rangle +
							\vert 4\rangle \vert 1\rangle + \cdots +
							\vert 15\rangle \vert 13\rangle)
\end{equation*}
\begin{equation*}
	= \frac{1}{4}((\vert 0\rangle + \vert 4\rangle + \vert 8\rangle + \vert 12\rangle)\vert 1\rangle
\end{equation*}
\begin{equation*}
	 							+ (\vert 1\rangle + \vert 5\rangle + \vert 9\rangle + \vert 13\rangle)\vert 7\rangle
\end{equation*}
\begin{equation*}
								+ (\vert 2\rangle + \vert 6\rangle + \vert 10\rangle + \vert 14\rangle)\vert 4\rangle
\end{equation*}
\begin{equation*}
								+ (\vert 3\rangle + \vert 7\rangle + \vert 11\rangle + \vert 15\rangle)\vert 13\rangle)
\end{equation*}

\texttt{Schritt 3:} \\
Die nächste Aufgabe wäre nun, die inverse Fouriertransformation darauf anzuwenden. Mit dieser kommt man auf folgendes Ergebnis.

\begin{equation*}
	\frac{1}{4}((\vert 0\rangle + \vert 4\rangle + \vert 8\rangle + \vert 12\rangle)\vert 1\rangle
\end{equation*}
\begin{equation*}
	 							+ (\vert 0\rangle + i\vert 4\rangle - \vert 8\rangle - i\vert 12\rangle)\vert 7\rangle
\end{equation*}
\begin{equation*}
								+ (\vert 0\rangle - \vert 4\rangle + \vert 8\rangle - \vert 12\rangle)\vert 4\rangle
\end{equation*}
\begin{equation*}
								+ (\vert 0\rangle - i\vert 4\rangle - \vert 8\rangle + i\vert 12\rangle)\vert 13\rangle)
\end{equation*}

\texttt{Schritt 4:} \\
In diesem Schritt sucht man sich jetzt, wie bereits erwähnt, geeignete $p$ heraus. In unserem Fall sind dies 0, 4, 8 und 12. Diese treten alle mit der Wahrscheinlichkeit von $\frac{1}{4}$ auf. \\

Mit den gegebenen Regeln betrachtet man nun die Konvergenzen. Für $\frac{0}{16}$ gibt es nur $\frac{0}{1}$, was aber nicht die gesuchte Periode preisgibt. Für die anderen $p$ sehen die Konvergenzen folgendermaßen aus
\begin{center}
	$\frac{4}{16} \rightarrow \frac{1}{4}, \frac{0}{1}$ \\
	$\frac{8}{16} \rightarrow \frac{1}{2}, \frac{0}{1}$ \\
	$\frac{12}{16} \rightarrow \frac{3}{4}, \frac{1}{1} \text{ und } \frac{0}{1}$
\end{center}
Hier fällt direkt auf, dass nur ein $q_i$ die Gleichung $a^{q_i} \equiv 1 \quad mod \quad n$ erfüllen kann. Hierbei handelt es sich um $q = 4$, womit wir auch die Periode gefunden haben. \\

An dieser Stelle würde jetzt die Periode, also die Zahl $4$ aus der Funktion \texttt{getOrderFromQuantumComputer()}, welche wir aufgerufen haben um die Ordnung einer Zahl zu erhalten,  wieder zurück in unseren Pseudocode gelangen und die Faktorisierung könnte weitergehen. \\

\texttt{Peter Shor} hat mit seinem Algorithmus einen riesen Schritt in der Informatik gemacht. Durch das Herausfinden, dass man die in der Faktorisierung zu berechnende Ordnung auf das Finden der Periode reduzieren kann erleichtert das Faktorisieren ungemein. Durch das Lösen der Fouriertransformation auf einem Quantencomputer erreicht man immense Beschleunigungen, welche in fast unglaublichen Laufzeiten resultieren. Dies funktioniert, da der Quantencomputer uns erlaubt hoch parallelisierte Anwendungen zu implementieren, die auf klassischen Rechnern nicht möglich sind. Ebenfalls ermöglicht uns der Algorithmus mit einer Wahrscheinlichkeit von 75\% einen nicht-trivialen Teiler durch $ggT(a^{\frac{r}{2}} \mp 1, n)$ zu finden. \\

Damit ist das Thema der Faktorisierung großer Zahlen mit Hilfe des Shor Algorithmus und Quantencomputern abgeschlossen und ich möchte mit einem kleinen Einblick in die Konsequenzen der schnellen Faktorisierung und einem Ausblick über weitere Entwicklungen meine Seminararbeit beenden.
